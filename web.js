import React, { Component } from 'react';
import {View} from 'react-native'
import { WebView } from 'react-native-webview';

export default class Web extends Component {
  render() {
    return (
      <WebView
        source={{uri: 'https://dentsa.tk/'}}
        style={{flex:1}}
        //userAgent={DeviceInfo.getUserAgent() + " - MYAPPNAME - android "}
        thirdPartyCookiesEnabled
        cacheEnabled
        sharedCookiesEnabled
        showsVerticalScrollIndicator={false}
        injectedJavaScript={`const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `}
        scalesPageToFit={false}
      />
    );
  }
}