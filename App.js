/*This is an Example of React Native Map*/
import React from 'react';
import { StyleSheet, Text, View , TextInput} from 'react-native';
import MapView, {Marker,Polyline,Circle,Polygon} from 'react-native-maps';
import MapViewdiractions from 'react-native-google-maps-directions';

const coordinates =[
  {
    latitude: 24.186848,
    longitude: 38.026428,
  }, 
  {
    latitude: 24.774265,
    longitude:  46.738586,
  },
  {
    latitude: 26.396790,
    longitude: 50.140400, 
  }
] ;
const GOOGLE_MAPS_APIKEY = '…';
export default class App extends React.Component {
  onRegionChange(region) {
    this.setState({ region });
  }
  
  render() {
    
    var mapStyle=[{"elementType": "geometry", "stylers": [{"color": "#242f3e"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#746855"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#242f3e"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#263c3f"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#6b9a76"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#38414e"}]},{"featureType": "road","elementType": "geometry.stroke","stylers": [{"color": "#212a37"}]},{"featureType": "road","elementType": "labels.text.fill","stylers": [{"color": "#9ca5b3"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#746855"}]},{"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#1f2835"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#f3d19c"}]},{"featureType": "transit","elementType": "geometry","stylers": [{"color": "#2f3948"}]},{"featureType": "transit.station","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "water","elementType": "geometry","stylers": [{"color": "#17263c"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#515c6d"}]},{"featureType": "water","elementType": "labels.text.stroke","stylers": [{"color": "#17263c"}]}];
    return (
      <View style={styles.container}>
        
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: 21.5423794, longitude: 	39.1979713,
            latitudeDelta: 20.0922,
            longitudeDelta: 20.0421,
          }}
          
        >
          
          <Polyline
		coordinates={[
      { latitude: 	24.6877308, longitude:  46.7218513 },//riyadh
      { latitude:   21.4266396, longitude:  39.8256302 },//mecca
      { latitude: 	21.5423794, longitude: 	39.1979713 },//jeddah
      { latitude:   24.0895405, longitude:  38.0617981 },//yanbu
      { latitude:   24.4686108, longitude:  39.6141701 },//medinh
      { latitude:   27.5218792, longitude:  41.690731 },//ha'il
      
		]}
    strokeColor="red" // fallback for when `strokeColors` is not supported by the map-provider
    //fillColor='green'
		strokeColors={[
			'green',
			'orange', // no color, creates a "long" gradient between the previous and next coordinate
			'green',
      'orange',
      'green',
      'orange',
      'green',
		
			
		]}
		strokeWidth={6}
	/>
          
        </MapView>
        
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    position:'absolute',
    top:2,
    left:2,
    right:2,
    bottom:2,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  map: {
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
  },
});